package com.example.recyclerviewcountrydemo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        AppCenter.start(application, "a0851f1e-c26d-4b27-8de0-8ec09952b58e",
                Analytics::class.java, Crashes::class.java)

        val country = mutableListOf(
                Country(R.drawable.flag_india, "India"),
                Country(R.drawable.flag_england, "England"),
                Country(R.drawable.flag_french, "French"),
                Country(R.drawable.flag_canada, "Canada"),
                Country(R.drawable.flag_afghanistan, "Afghanistan"),
                Country(R.drawable.flag_ireland, "Ireland"),
                Country(R.drawable.flag_bulgaria, "Bulgaria"),
                Country(R.drawable.flag_bermuda, "Bermuda"),
                Country(R.drawable.flag_jamaica, "Jamaica"),
                Country(R.drawable.flag_bangladesh, "Bangladesh"),
                Country(R.drawable.flag_bhutan, "Bhutan"),
                Country(R.drawable.flag_israel, "Israel"),
                Country(R.drawable.flag_indonesia, "Indonesia"),
                Country(R.drawable.flag_colombia, "Colombia"),
                Country(R.drawable.flag_germany, "Germany"),
                Country(R.drawable.flag_japan, "Japan"),
                Country(R.drawable.flag_finland, "Finland"),
                Country(R.drawable.flag_iran, "Iran"),
                Country(R.drawable.flag_italy, "Italy"),
                Country(R.drawable.flag_guyana, "Guyana"),
                Country(R.drawable.flag_kenya, "Kenya")

        )

        val adapter = CountryAdapter(country)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        val dividerItemDecoration = DividerItemDecoration(this, RecyclerView.VERTICAL)
        recyclerView.addItemDecoration(dividerItemDecoration)

        adapter.notifyItemChanged(country.size)

    }
}