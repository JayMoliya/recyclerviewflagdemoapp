package com.example.recyclerviewcountrydemo

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_items.view.*

class CountryAdapter(private val country: MutableList<Country>) : RecyclerView.Adapter<CountryAdapter.MyViewHolder>(){

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_items,parent,false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.itemView.apply {
            tvCountryName.text = country[position].countryName
            imgCountryFlag.setImageResource(country[position].countryFlag)

            setOnClickListener {
                Toast.makeText(context,"You clicked on ${tvCountryName.text}",Toast.LENGTH_SHORT).show()
            }
        }

    }

    override fun getItemCount(): Int {
        return country.size
    }

}